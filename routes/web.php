<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Route::get('/', 'WebController@index')->name('index');

Route::group(['prefix' => '/cart', 'middleware' => 'isLogin'], function () {
	Route::get('/', 'CartController@showCart')->name('cart');
	Route::post('/{p_id}/add', 'CartController@addItem')->name('cart.add');
	Route::delete('/{i_id}/delete', 'CartController@deleteItem')->name('cart.delete');
	Route::patch('/{i_id}/edit', 'CartController@editItem')->name('cart.edit');
	Route::post('/checkout', 'OrderController@checkout')->name('cart.checkout');
});

Route::group(['prefix' => '/store'], function () {
	Route::get('/', 'WebController@showStore')->name('store');
});

Route::group(['prefix' => 'member', 'middleware' => 'isLogin'], function () {
	Route::get('/', 'MemberController@center')->name('member');
	Route::get('/tos', 'MemberController@tos')->name('member.tos');
	Route::get('/order', 'OrderController@showOrder')->name('member.order');	
	Route::get('/order/{o_id}', 'OrderController@showOrderDetail')->name('member.order.d');
	Route::post('/order/pay', 'OrderController@payBilling')->name('member.order.pay');
	Route::get('/order/payByWallet/{o_id}', 'OrderController@payBillingByWallet')->name('member.order.paybywallet');
	Route::post('/order/payByWalletc', 'OrderController@payBillingByWalletc')->name('member.order.paybywalletc');
	Route::post('/refund', 'MemberController@Refund')->name('member.refund');
	Route::get('/logout', 'MemberController@logout')->name('member.logout');
});

	Route::get('/member/login', 'MemberController@login')->name('member.login');
	Route::post('/member/login', 'MemberController@loginc')->name('member.loginc');
	Route::get('/member/reg', 'MemberController@reg')->name('member.reg');
	Route::post('/member/reg', 'MemberController@regc')->name('member.regc');

Route::group(['prefix' => 'admin', 'middleware' => 'isLogin:admin'], function () {
	Route::get('/', 'AdminController@admin')->name('admin');
	Route::get('/order', 'AdminController@showOrder')->name('admin.order');
	Route::get('/order/{o_id}', 'AdminController@showOrderDetail')->name('admin.order.d');
	Route::resource('/product', 'ProductController');
	Route::post('/refund', 'AdminController@Refund')->name('admin.refund');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
