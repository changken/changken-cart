<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_infos', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('order_id')->unsigned();
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->string('');//出貨狀態
			$table->string('')->nullable();//出貨追蹤碼
            $table->timestamp('_at')->nullable();//出貨時間
			$table->string('');//收件人姓名
			$table->string('');//收件人電郵
			$table->string('')->nullable();//收件人電話
			$table->string('')->nullable();//收件人地址
			$table->text('note')->nullable();//出貨備註
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_infos');
    }
}
