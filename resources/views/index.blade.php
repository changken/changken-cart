@extends('tpl.main')

@section('title', 'changken的商店')

@section('content')
		<div class="jumbotron">
			<h1>你好啊！歡迎來到changken的商店！</h1>
			<p>簡單 迅速 安全</p>
			<p><a class="btn btn-primary btn-lg" href="{{ url('/store') }}" role="button">購物去</a></p>
		</div>
@endsection