@extends('tpl.main')

@section('title', '註冊')

@section('content')
		<form action="" method="post" role="form">
			<div class="form-group">
				<label for="name">帳號:</label>
				<input type="text" name="name" id="name" class="form-control">
			</div>
			<div class="form-group">
				<label for="email">電子郵件:</label>
				<input type="text" name="email" id="email" class="form-control">
			</div>
			<div class="form-group">
				<label for="pw">密碼:</label>
				<input type="password" name="pw" id="pw" class="form-control">
			</div>
			<div class="form-group">
				<label for="pw2">確認密碼:</label>
				<input type="password" name="pw2" id="pw2" class="form-control">
			</div>
			{{ csrf_field() }}
			<button type="submit" class="btn btn-primary">註冊</button>
		</form>
@endsection