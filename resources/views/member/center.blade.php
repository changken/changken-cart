@extends('tpl.main')

@section('title', '會員中心')

@section('content')
		@if(Auth::check())
			<table class="table">
				<tr>
					<td>您的大名:</td>
					<td>{{ $user->name }}</td>
				</tr>
				<tr>
					<td>您的電郵:</td>
					<td>{{ $user->email }}</td>
				</tr>
				<tr>
					<td>您的等級:</td>
					@if($user->userinfo->level == 'admin')
						<td><span class="label label-info">管理員</td>
					@else
						<td><span class="label label-info">一般會員</td>
					@endif
				</tr>
				<tr>
					<td>您的帳號狀態:</td>
					@if($user->userinfo->state == 'open')
						<td><span class="label label-success">已啟用</td>
					@else
						<td><span class="label label-danger">未啟用</td>
					@endif
				</tr>
				<tr>
					<td>註備:</td>
					<td>changken 錢包相關業務業已轉交給changken 錢包處理。<br/>
					客戶一樣可以在本站使用錢包，但請先在changken 錢包註冊並儲值後再行使用！<br/>
					於支付前，一定要先登入錢包！不然會一直跳支付失敗的頁面唷~
					</td>
				</tr>
			</table>
		@else
			<h1 class="text-center">您尚未登入</h1>		
		@endif
@endsection