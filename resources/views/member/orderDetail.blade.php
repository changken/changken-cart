@extends('tpl.main')

@section('title', '詳細訂購資訊')

@section('content')
		<div class="alert alert-info">
			<p class="text-center">
				<b>提示:</b>這個網頁等同於收據！您可以隨時列印此頁來當作證明！(面交除外)
			</p>
		</div>
		<div class="alert alert-warning">
			<p class="text-center">
				<b>注意:</b>如果您的帳號有頻繁退款的情形，視情況得拒絕您的退款或暫停您的帳號！<br/>
				<b>注意:</b>帳單金額為「免費」也需要按下「支付」！否則帳單不成立！<br/>
				<b>注意:</b>使用changken 錢包支付時，請務必先登入！<br/>
				<b>注意:</b>現在不支援秒退款服務！<br/>
			</p>
		</div>
		@if($Order->billing->status == "已支付(退款被拒)")
			<div class="alert alert-danger">
				<p class="text-center">
					<b>警告！</b>您的退款請求已被拒絕！這意味著您不可以再次請求退款！
				</p>
			</div>			
		@endif
		<h2 class="text-left">訂購項目:</h2>
		<table class="table">
			<tr>
				<td>品名</td>
				<td>圖片</td>
				<td>描述</td>
				<td>價格</td>
				<td>數量</td>
			</tr>
			@foreach($items as $item)
				<tr>
					<td>{{ $item->product->name }}</td>
					<td><img src="{{ $item->product->img_url }}" class="img-responsive" alt="此商品沒有圖片！"></td>
					<td>{{ $item->product->description }}</td>
					@if($item->product->price == 0)
						<td><span class="label label-success" style="font-size:14px;">免費！</span></td>
					@else
						<td>NT$ {{ $item->product->price }}</td>
					@endif
					<td>{{ $item->amount }}</td>
				</tr>				
			@endforeach
		</table>
		{{ $items->links() }}
		<h2 class="text-left">帳單:</h2>
		<table class="table">
			<tr>
				<td>編號</td>
				<td>{{ $Order->billing->id }}</td>
			</tr>
			<tr>
				<td>合計</td>
				@if($Order->billing->price == 0)
					<td><span class="label label-success" style="font-size:14px;">免費！</span></td>
				@else
					<td>NT$ {{ $Order->billing->price }}</td>
				@endif
			</tr>
			<tr>
				<td>支付狀態</td>
				@if($Order->billing->status == "已支付")
					<td><h4><span class="label label-success">{{ $Order->billing->status }}</span></h4></td>
				@elseif($Order->billing->status == "已支付(退款被拒)")
					<td><h4><span class="label label-success">{{ $Order->billing->status }}</span></h4></td>
				@elseif($Order->billing->status == "待確認")
					<td><h4><span class="label label-warning">{{ $Order->billing->status }}</span></h4></td>
				@elseif($Order->billing->status == "未支付")
					<td><h4><span class="label label-danger">{{ $Order->billing->status }}</span></h4></td>
				@elseif($Order->billing->status == "已取消")
					<td><h4><span class="label label-default">{{ $Order->billing->status }}</span></h4></td>
				@else
					<td><h4><span class="label label-info">{{ $Order->billing->status }}</span></h4></td>
				@endif
			</tr>
			<tr>
				<td>支付方式</td>
				<td>{{ $Order->billing->payway }}</td>
			</tr>
			<tr>
				<td>註備</td>
				<td>{!! $Order->billing->note !!}</td>
			</tr>
			<tr>
				<td>付款於</td>
				<td>{{ $Order->billing->pay_at }}</td>
			</tr>
			<tr>
				<td>訂購日期</td>
				<td>{{ $Order->created_at }}</td>
			</tr>
			<tr>
				<td>退款(對於訂單很不滿意?)</td>
				<td>
					<form action="{{ url('/member/refund') }}" method="post" class="form-inline" role="form">
						<input type="hidden" name="order_id" value="{{ $Order->id }}">
						<input type="hidden" name="billing_id" value="{{ $Order->billing->id }}">
						{{ csrf_field() }}
						@if($Order->billing->status == "已支付" or $Order->billing->status == "待確認")
							<button type="submit" class="btn btn-danger">請求退款</button> 
						@else
							<button type="submit" class="btn btn-danger" disabled="disabled">請求退款</button> 
						@endif
					</form>
				</td>
			</tr>
		</table>
		@if($Order->billing->status == "未支付")
			<h2 class="text-left">支付帳單:</h2>
			<p class="text-right">
				@if($Order->billing->price == 0)
					<span style="font-size:30px;">合計:<span class="label label-success">免費！</span></span><br/>
				@else
					<span style="font-size:30px;">合計:NT${{ $Order->billing->price }}</span><br/>
				@endif
				支付方式:
			</p>
			<form action="{{ url('/member/order/pay') }}" method="post" class="text-right form-horizontal" role="form">
				<input type="hidden" name="billing_id" value="{{ $Order->billing->id }}">
				@if($Order->billing->price == 0)
					<div class="radio">
						<label>
							<input type="radio" name="payway" value="free">
							免費產品
						</label>
					</div>
				@else
					<div class="radio">
						<label>
							<input type="radio" name="payway" value="changken_wallet">
							Changken Wallet
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="payway" value="face2face">
							面交(限實體商品)
						</label>
					</div>
					<div class="radio disabled">
						<label>
							<input type="radio" name="payway" value="free" disabled>
							免費產品(不可用)
						</label>
					</div>
				@endif
				{{ csrf_field() }}
				<p>按下「支付」即視同您已詳閱並完全同意本站的服務條款！</p>
				<button type="submit" class="btn btn-primary btn-lg">支付</button>
			</form>
		@endif
@endsection