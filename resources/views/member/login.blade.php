@extends('tpl.main')

@section('title', '登入')

@section('content')
		<div class="alert alert-info">
			<p class="text-center">沒有帳號？<a href="{{ url('/member/reg') }}">馬上註冊！</a></p>
		</div>
		<form action="" method="post" role="form">
			<div class="form-group">
				<label for="name">帳號:</label>
				<input type="text" name="name" id="name" class="form-control" placeholder="username">
			</div>
			<div class="form-group">
				<label for="pw">密碼:</label>
				<input type="password" name="pw" id="pw" class="form-control" placeholder="password">
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="remember">記住我
				</label>
			</div>
			{{ csrf_field() }}
			<button type="submit" class="btn btn-primary">登入</button>
		</form>
@endsection