<!DOCTYPE html>
<html lang="zh-TW">
	<head>
		<title>支付中...</title>
		<meta charset="utf8">
	</head>
	
	<body>
		<form id="form" method="post" action="{{ $Api_url }}"><br/>
			<input type="hidden" name="Return_url" value='{{ $Return_url }}'><br/>
			<input type="hidden" name="Api_id" value='{{ $Api_id }}'><br/>
			<input type="hidden" name="Api_token" value='{{ $Api_token }}'><br/>
			<input type="hidden" name="Date" value='{{ $Date }}'><br/>
			<input type="hidden" name="Item" value='購物花費  帳單編號:{{ $billing->id }}'><br/>
			<input type="hidden" name="Total" value='{{ $billing->price }}'><br/>
			<textarea name="Note" style="display:none">支付人:{{ $billing->user->name }} 訂購編號:{{ $billing->order->id }}  帳單編號:{{ $billing->id }}</textarea><br/>
		</form><br/>
		<script>
			form.submit();
		</script>
	</body>
</html>