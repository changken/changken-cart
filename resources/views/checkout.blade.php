@extends('tpl.main')

@section('title', '結帳')

@section('content')
		<div class="alert alert-success">
			<p class="text-center"><b>訂購成功！</b>您可以到訂購列表查看更詳細的訂購資料！</p>
		</div>
		<table class="table">
			<tr>
				<td>品名</td>
				<td>圖片</td>
				<td>描述</td>
				<td>價格</td>
				<td>數量</td>
			</tr>
			@foreach($items as $item)
				<tr>
					<td>{{ $item->product->name }}</td>
					<td><img src="{{ $item->product->img_url }}" class="img-responsive" alt="此商品沒有圖片！"></td>
					<td>{{ $item->product->description }}</td>
					@if($item->product->price == 0)
						<td><span class="label label-success" style="font-size:14px;">免費！</span></td>
					@else
						<td>NT$ {{ $item->product->price }}</td>
					@endif
					<td>{{ $item->amount }}</td>
				</tr>				
			@endforeach
		</table>
		<p class="text-right">
			<span style="font-size:30px;">
				@if($Order->billing->price == 0)
					合計:<span class="label label-success" style="font-size:20px;">免費！</span><br/>
				@else
					合計:NT${{ $Order->billing->price }}<br/>
				@endif
				<a href="{{ url('/member/order/'.$Order->id)}}" class="btn btn-success btn-lg">支付此帳單！</a>
			</span>
		</p>
@endsection