	<nav>
		<ul class="nav nav-tabs">
			<li role="presentation"><a href="{{ url('/') }}">首頁</a></li>
			<li role="presentation"><a href="{{ url('/admin') }}">管理中心</a></li>
			<li role="presentation" class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
					商品管理 <span class="caret"></span>
				</a>
				<ul class="dropdown-menu" role="menu">
					<li role="presentation"><a href="{{ url('/admin/product') }}">商品列表</a></li>
					<li role="presentation"><a href="{{ url('/admin/product/create') }}">新增商品</a></li>
				</ul>
			</li>
			<li role="presentation"><a href="{{ url('/admin/order') }}">訂購列表</a></li>
		</ul>
	</nav>