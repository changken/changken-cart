<!doctype html>
<html lang="zh-TW">

<head>
	<title>@yield('title')</title>
	<meta charset="utf-8">
	@include('tpl-admin.head')
</head>

<body>
	<div class="container">
		<h1 class="text-center">@yield('title')</h1>
		@include('tpl-admin.nav')
		<hr><size=5>
		@section('content')
		@show
		<hr><size=5>
		@include('tpl-admin.footer')
	</div>
</body>

</html>