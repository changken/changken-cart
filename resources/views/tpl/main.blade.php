<!doctype html>
<html lang="zh-TW">

<head>
	<title>@yield('title')</title>
	<meta charset="utf-8">
	@include('tpl.head')
</head>

<body>
	<div class="container">
		<h1 class="text-center">@yield('title')</h1>
		@include('tpl.nav')
		<hr><size=5>
		@section('content')
		@show
		<hr><size=5>
		@include('tpl.footer')
	</div>
</body>

</html>