	<nav>
		<ul class="nav nav-tabs">
			<li role="presentation"><a href="{{ url('/') }}">首頁</a></li>
			<li role="presentation"><a href="{{ url('/store') }}">商店</a></li>
			<li role="presentation"><a href="{{ url('/cart') }}">購物車</a></li>
			<li role="presentation"><a href="{{ url('/member') }}">會員中心</a></li>
			<li role="presentation" class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
					功能表 <span class="caret"></span>
				</a>
				<ul class="dropdown-menu" role="menu">
					<li role="presentation"><a href="{{ url('/member/order') }}">訂購列表</a></li>
				</ul>
			</li>
			@if(Auth::check())
				<li role="presentation"><a href="{{ url('/member/logout') }}">登出【{{ Auth::user()->name }}】</a></li>
			@else
				<li role="presentation"><a href="{{ url('/member/login') }}">登入</a></li>
			@endif
		</ul>
	</nav>