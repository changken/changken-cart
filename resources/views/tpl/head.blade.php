	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="{{ asset('bootstrap/3.3.7/css/bootstrap.min.css') }}">
	<!-- Optional theme -->
	<link rel="stylesheet" href="{{ asset('bootstrap/3.3.7/css/bootstrap-theme.min.css') }}">
	<!--jQuery-->
	<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="{{ asset('bootstrap/3.3.7/js/bootstrap.min.js') }}"></script>
	<!--Custom CSS-->
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<meta name="viewport" content="width=device-width,initial-scale=1">