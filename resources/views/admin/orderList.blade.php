@extends('tpl-admin.main')

@section('title', '管理中心 - 訂購列表')

@section('content')
		<table class="table">
			<tr>
				<td>訂單編號</td>
				<td>訂購人</td>
				<td>訂購日期</td>
				<td>價格</td>
				<td>支付狀態</td>
				<td>付款於</td>
				<td>詳細</td>
			</tr>
			@foreach($Orders as $Order)
				<tr>
					<td>{{ $Order->id }}</td>
					<td>{{ $Order->user->name }}</td>
					<td>{{ $Order->created_at }}</td>
					<td>NT$ {{ $Order->billing->price }}</td>
					@if($Order->billing->status == "已支付")
						<td><h4><span class="label label-success">{{ $Order->billing->status }}</span></h4></td>
					@elseif($Order->billing->status == "已支付(退款被拒)")
						<td><h4><span class="label label-success">{{ $Order->billing->status }}</span></h4></td>
					@elseif($Order->billing->status == "待確認")
						<td><h4><span class="label label-warning">{{ $Order->billing->status }}</span></h4></td>
					@elseif($Order->billing->status == "未支付")
						<td><h4><span class="label label-danger">{{ $Order->billing->status }}</span></h4></td>
					@elseif($Order->billing->status == "已取消")
						<td><h4><span class="label label-default">{{ $Order->billing->status }}</span></h4></td>
					@else
						<td><h4><span class="label label-info">{{ $Order->billing->status }}</span></h4></td>
					@endif
					<td>{{ $Order->billing->pay_at }}</td>
					<td><a href="{{ url('/admin/order/'.$Order->id)}}" class="btn btn-success">詳細</a></td>
				</tr>				
			@endforeach
		</table>
		{{ $Orders->links() }}
@endsection