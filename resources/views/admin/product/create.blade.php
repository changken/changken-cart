@extends('tpl-admin.main')

@section('title', '管理中心 - 新增商品')

@section('content')
			<form action="{{ url('/admin/product') }}" method="post" role="form">
				<div class="form-group">
					<label for="user_id">使用者ID:</label>
					<input type="text" name="user_id" id="user_id" class="form-control" aria-describedby="helpBlock" value="{{ Auth::id() }}" readonly>
					<span class="help-block" id="helpBlock">e.g. 1 => ken(管理員)</span>
				</div>
				<div class="form-group">
					<label for="name">商品名:</label>
					<input type="text" name="name" id="name" class="form-control">
				</div>
				<div class="form-group">
					<label for="img_url">圖片網址:</label>
					<input type="text" name="img_url" id="img_url" class="form-control">
				</div>
				<div class="form-group">
					<label for="price">價格:</label>
					<input type="text" name="price" id="price" class="form-control">
				</div>
				<div class="form-group">
					<label for="description">詳細描述:</label>
					<textarea name="description" id="description" class="form-control" rows="3"></textarea>
				</div>
				<button type="submit" class="btn btn-primary">新增</button>
				{{ csrf_field() }}
			</form>
@endsection