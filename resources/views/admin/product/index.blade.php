@extends('tpl-admin.main')

@section('title', '管理中心 - 商品列表')

@section('content')
		<table class="table">
			<tr>
				<td>品名</td>
				<td>圖片</td>
				<td>描述</td>
				<td>價格</td>
				<td>編輯</td>
				<td>移除</td>
			</tr>
			@foreach($Products as $Product)
				<tr>
					<td>{{ $Product->name }}</td>
					<td><img src="{{ $Product->img_url }}" class="img-responsive" alt="此商品沒有圖片！"></td>
					<td>{{ $Product->description }}</td>
					@if($Product->price == 0)
						<td><span class="label label-success" style="font-size:14px;">免費！</span></td>
					@else
						<td>NT$ {{ $Product->price }}</td>
					@endif
					<td>
						<a href="{{ url('/admin/product/'.$Product->id .'/edit') }}" class="btn btn-warning">編輯</a>
					</td>
					<td>
						<form action="{{ url('/admin/product/'.$Product->id) }}" method="post" class="form-inline" role="form">
							<input type="hidden" name="_method" value="DELETE">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger">移除</button>
						</form>
					</td>
				</tr>				
			@endforeach
		</table>
		{{ $Products->links() }}
@endsection