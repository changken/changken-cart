@extends('tpl-admin.main')

@section('title', '管理中心 - 編輯商品')

@section('content')
			<form action="{{ url('/admin/product/'.$Product->id ) }}" method="post" role="form">
				<div class="form-group">
					<label for="id">商品ID:</label>
					<input type="text" name="id" id="id" class="form-control" value="{{ $Product->id }}" readonly>
				</div>
				<div class="form-group">
					<label for="user_id">使用者ID:</label>
					<input type="text" name="user_id" id="user_id" class="form-control" aria-describedby="helpBlock" value="{{ $Product->user_id }}" readonly>
					<span class="help-block" id="helpBlock">e.g. 1 => ken(管理員)</span>
				</div>
				<div class="form-group">
					<label for="name">商品名:</label>
					<input type="text" name="name" id="name" class="form-control" value="{{ $Product->name }}">
				</div>
				<div class="form-group">
					<label for="img_url">圖片網址:</label>
					<input type="text" name="img_url" id="img_url" class="form-control" value="{{ $Product->img_url }}">
				</div>
				<div class="form-group">
					<label for="price">價格:</label>
					<input type="text" name="price" id="price" class="form-control" value="{{ $Product->price }}">
				</div>
				<div class="form-group">
					<label for="description">詳細描述:</label>
					<textarea name="description" id="description" class="form-control" rows="3">{{ $Product->description }}</textarea>
				</div>
				<div class="form-group">
					<label for="created_at">新增時間:</label>
					<input type="text" name="created_at" id="created_at" class="form-control" value="{{ $Product->created_at }}" readonly>
				</div>
				<div class="form-group">
					<label for="updated_at">最後更新:</label>
					<input type="text" name="updated_at" id="updated_at" class="form-control" value="{{ $Product->updated_at }}" readonly>
				</div>
				<button type="submit" class="btn btn-warning">編輯</button>
				<input type="hidden" name="_method" value="PATCH">
				{{ csrf_field() }}
			</form>
@endsection