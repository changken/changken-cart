@extends('tpl-admin.main')

@section('title', '管理中心 - 詳細訂購資訊')

@section('content')
		<p class="text-left">訂購項目:</p>
		<table class="table">
			<tr>
				<td>品名</td>
				<td>圖片</td>
				<td>描述</td>
				<td>價格</td>
				<td>數量</td>
			</tr>
			@foreach($items as $item)
				<tr>
					<td>{{ $item->product->name }}</td>
					<td><img src="{{ $item->product->img_url }}" class="img-responsive" alt="此商品沒有圖片！"></td>
					<td>{{ $item->product->description }}</td>
					<td>NT$ {{ $item->product->price }}</td>
					<td>{{ $item->amount }}</td>
				</tr>				
			@endforeach
		</table>
		{{ $items->links() }}
		<p class="text-left">帳單:</p>
		<table class="table">
			<tr>
				<td>編號</td>
				<td>{{ $Order->billing->id }}</td>
			</tr>
			<tr>
				<td>訂購人</td>
				<td>{{ $Order->user->name }}</td>
			</tr>
			<tr>
				<td>合計</td>
				<td>NT$ {{ $Order->billing->price }}</td>
			</tr>
			<tr>
				<td>支付狀態</td>
				@if($Order->billing->status == "已支付")
					<td><h4><span class="label label-success">{{ $Order->billing->status }}</span></h4></td>
				@elseif($Order->billing->status == "已支付(退款被拒)")
					<td><h4><span class="label label-success">{{ $Order->billing->status }}</span></h4></td>
				@elseif($Order->billing->status == "待確認")
					<td><h4><span class="label label-warning">{{ $Order->billing->status }}</span></h4></td>
				@elseif($Order->billing->status == "未支付")
					<td><h4><span class="label label-danger">{{ $Order->billing->status }}</span></h4></td>
				@elseif($Order->billing->status == "已取消")
					<td><h4><span class="label label-default">{{ $Order->billing->status }}</span></h4></td>
				@else
					<td><h4><span class="label label-info">{{ $Order->billing->status }}</span></h4></td>
				@endif
			</tr>
			<tr>
				<td>支付方式</td>
				<td>{{ $Order->billing->payway }}</td>
			</tr>
			<tr>
				<td>註備</td>
				<td>{!! $Order->billing->note !!}</td>
			</tr>
			<tr>
				<td>付款於</td>
				<td>{{ $Order->billing->pay_at }}</td>
			</tr>
			<tr>
				<td>訂購日期</td>
				<td>{{ $Order->created_at }}</td>
			</tr>
			<tr>
				<td>退款動作</td>
				<td>
					<form action="{{ url('/admin/refund') }}" method="post" class="form-inline" role="form">
						<input type="hidden" name="response" value="Approval">
						<input type="hidden" name="order_id" value="{{ $Order->id }}">
						<input type="hidden" name="billing_id" value="{{ $Order->billing->id }}">
						{{ csrf_field() }}
						@if($Order->billing->status == "請求退款中")
							<button type="submit" class="btn btn-success">批准</button> 
						@else
							<button type="submit" class="btn btn-success" disabled="disabled">批准</button> 
						@endif
					</form>
					<form action="{{ url('/admin/refund') }}" method="post" class="form-inline" role="form">
						<input type="hidden" name="response" value="Refuse">
						<input type="hidden" name="order_id" value="{{ $Order->id }}">
						<input type="hidden" name="billing_id" value="{{ $Order->billing->id }}">
						{{ csrf_field() }}
						@if($Order->billing->status == "請求退款中")
							<button type="submit" class="btn btn-danger">拒絕</button> 
						@else
							<button type="submit" class="btn btn-danger" disabled="disabled">拒絕</button> 
						@endif
					</form>
				</td>
			</tr>
		</table>
@endsection