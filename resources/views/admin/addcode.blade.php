@extends('tpl-admin.main')

@section('title', '管理中心 - 新增錢包代碼')

@section('content')
			<form action="" method="post" role="form">
				<div class="form-group">
					<label for="key">錢包代碼:</label>
					<input type="text" name="key" id="key" size="11" class="form-control" placeholder="XXX-XXX-XXX" aria-describedby="helpBlock">
					<span class="help-block" id="helpBlock">e.g. 44L-2KK-ONG 若此格留空，則隨機產生一組！</span>
				</div>
				<div class="form-group">
					<label for="value">價值:</label>
					<input type="text" name="value" id="value" class="form-control">
				</div>
				<button type="submit" class="btn btn-primary">新增</button>
				{{ csrf_field() }}
			</form>
@endsection