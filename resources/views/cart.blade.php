@extends('tpl.main')

@section('title', '購物車')

@section('content')
		<table class="table">
			<tr>
				<td>品名</td>
				<td>圖片</td>
				<td>描述</td>
				<td>價格</td>
				<td>數量(編輯)</td>
				<td>移除</td>
			</tr>
			@foreach($items as $item)
				<tr>
					<td>{{ $item->product->name }}</td>
					<td><img src="{{ $item->product->img_url }}" class="img-responsive" alt="此商品沒有圖片！"></td>
					<td>{{ $item->product->description }}</td>
					@if($item->product->price == 0)
						<td><span class="label label-success" style="font-size:14px;">免費！</span></td>
					@else
						<td>NT$ {{ $item->product->price }}</td>
					@endif
					<td>
						<form action="{{ url('/cart/'.$item->id.'/edit') }}" method="post" class="form-inline" role="form">
							<input type="text" name="amount" value="{{ $item->amount }}" class="form-control">
							{{ method_field('PATCH') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-warning">編輯</button>
						</form>
					</td>
					<td>
						<form action="{{ url('/cart/'.$item->id.'/delete') }}" method="post" class="form-inline" role="form">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger">移除</button>
						</form>
					</td>
				</tr>				
			@endforeach
		</table>
		{{ $items->links() }}
		<p class="text-right">
			@if($total == 0)
				合計:<span class="label label-success" style="font-size:20px;">免費！</span>
			@else
				<span style="font-size:30px;">合計:NT${{ $total }}</span>
			@endif
		</p>
		<form action="{{ url('/cart/checkout') }}" method="post" class="text-right form-horizontal" role="form">
			{{ csrf_field() }}
			<p>按下「結帳」即視同您已詳閱並完全同意本站的服務條款！</p>
			<button type="submit" class="btn btn-primary btn-lg">結帳</button>
		</form>
@endsection