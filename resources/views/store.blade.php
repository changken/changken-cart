@extends('tpl.main')

@section('title', '商店')

@section('content')
		<div class="alert alert-info">
			<p class="text-center"><b>提示:</b>如須購買請輸入數量(限數字)後再按「加入購物車」！</p>
		</div>
		<table class="table">
			<tr>
				<td>品名</td>
				<td>圖片</td>
				<td>描述</td>
				<td>價格</td>
				<td>數量(加入購物車)</td>
			</tr>
			@foreach($Products as $Product)
				<tr>
					<td>{{ $Product->name }}</td>
					<td><img src="{{ $Product->img_url }}" class="img-responsive" alt="此商品沒有圖片！"></td>
					<td>{{ $Product->description }}</td>
					@if($Product->price == 0)
						<td><span class="label label-success" style="font-size:14px;">免費！</span></td>
					@else
						<td>NT$ {{ $Product->price }}</td>
					@endif
					<td>
						<form action="{{ url('/cart/'.$Product->id.'/add') }}" method="post" class="form-inline" role="form">
							<input type="text" name="amount" class="form-control" placeholder="1">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-success">加入購物車</button>
						</form>
					</td>
				</tr>				
			@endforeach
		</table>
		{{ $Products->links() }}
@endsection