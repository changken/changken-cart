@extends('tpl.main')

@section('title', '關於我')

@section('content')
		<div class="alert alert-danger">
			<p class="text-center"><b>警告！</b>此頁面尚未完工！</p>
		</div>
		<p class="text-center">你可以<a href="https://changken.org/who-is-changken" target="_blank">到這邊</a>查看</p>
@endsection