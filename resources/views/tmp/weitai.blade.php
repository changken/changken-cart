@extends('tpl.main')

@section('title', '韋泰 - 由changken代管')

@section('content')
		<div class="alert alert-danger">
			<p class="text-center"><b>警告！</b>此頁面尚未完工！</p>
		</div>
		<p class="text-center">此頁面為韋泰茶具茶葉商行的官方網站！<br/>
		於原官網不可訪問時，由本頁面代替之。<br/>
		電郵:weitai168168@gmail.com(客服信箱)<br/>
		FB:<a href="https://www.facebook.com/weitai168.com.tw">https://www.facebook.com/weitai168.com.tw</a>(即時客服)<br/>
		電話:(02)26707622</p>
@endsection