@extends('tpl.main')

@section('title', '三峽媽媽味便當 x changken 商店')

@section('content')
		<p class="text-center">健康 好吃 有機的三峽媽媽味便當現在可以在changken 商店上購買！<br/>
		因此changken 商店推出限時優惠，只要在changken 商店上使用changken 錢包購買，每個便當只要NT$39！<br/>
		changken 錢包有著免去帶錢 找零 立即支付等好處！<br/>
		還不趕快去試試？
		</p>
@endsection