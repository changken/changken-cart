<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public function User()
	{
		return $this->belongsTo(User::class);
	}
	
	public function CartItem()
	{
		return $this->hasMany(CartItem::class);
	}
}
