<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    public function User()
	{
		return $this->belongsTo(User::class);
	}
	   
	public function Order()
	{
		return $this->belongsTo(Order::class);
	}
}
