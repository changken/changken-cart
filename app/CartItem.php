<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    public function Cart()
	{
		return $this->belongsTo(Cart::class);
	}
	
	public function Product()
	{
		return $this->belongsTo(Product::class);
	}
}
