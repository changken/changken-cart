<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public function Product()
	{
		return $this->hasMany(Product::class);
	}

	public function Cart()
	{
		return $this->hasMany(Cart::class);
	}

	public function Order()
	{
		return $this->hasMany(Order::class);
	}

	public function Billing()
	{
		return $this->hasMany(Billing::class);
	}

	public function Wallet()
	{
		return $this->hasOne(Wallet::class);
	}

	public function Userinfo()
	{
		return $this->hasOne(UserInfo::class);
	}
}
