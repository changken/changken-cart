<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function User()
	{
		return $this->belongsTo(User::class);
	}
	
	public function CartItem()
	{
		return $this->hasOne(CartItem::class);
	}
	
	public function OrderItem()
	{
		return $this->hasOne(OrderItem::class);
	}
}
