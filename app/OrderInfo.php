<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderInfo extends Model
{
    public function Order()
	{
		return $this->belongsTo(Order::class);
	}
}
