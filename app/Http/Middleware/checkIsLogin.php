<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Gate;

class checkIsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $level = null)
    {		
		if(!Auth::check())
		{
			return redirect('/member/login');
		}
		else if($level == "admin" && Gate::denies('view-admin'))
		{
			return redirect('/member');
		}
		else
		{
			//
		}
		
        return $next($request);
    }
}
