<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\CartItem;
use App\Product;
use Auth;

class CartController extends Controller
{
	//加入新商品
	public function addItem($p_id,Request $request)
	{
		$Cart = Cart::where('user_id', Auth::id())->first();
		
		if(!$Cart)
		{
			$Cart = new Cart;
			$Cart->user_id = Auth::id();
			$Cart->save();
		}
		
		$CartItem = new CartItem;
		$CartItem->cart_id = $Cart->id;
		$CartItem->product_id = $p_id;
		$CartItem->amount = $request->input('amount');
		$CartItem->save();

		return redirect('/cart');
	}
	
	//顯示購物車
    public function showCart()
	{
		$Cart = Cart::where('user_id', Auth::id())->first();
		
		if(!$Cart)
		{
			$Cart = new Cart;
			$Cart->user_id = Auth::id();
			$Cart->save();
		}
		
		$items = $Cart->cartitem;
		$total = 0;
		foreach($items as $item)
		{
			$total += $item->product->price * $item->amount; 
		}
		
		return view('cart', [
			'items' => $Cart->cartitem()->paginate(5),
			'total' => $total
		]);
	}
	
	//移除商品
	public function deleteItem($i_id)
	{
		CartItem::destroy($i_id);
		
		return redirect('/cart');
	}
	
	//修改商品數量
	public function editItem($i_id,Request $request)
	{
		$CartItem = CartItem::findOrFail($i_id);
		$CartItem->amount = $request->input('amount');
		$CartItem->save();
		
		return redirect('/cart');
	}
}
