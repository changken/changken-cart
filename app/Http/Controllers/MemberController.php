<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserInfo;
use App\Billing;
use Auth;
use Gate;

class MemberController extends Controller
{	
	//會員中心
	public function center()
	{
		$user = User::findorFail(Auth::id());
		
		return view('member.center', [
			'user' => $user,
		]);		
	}	
	
	public function tos()
	{
		return view('member.tos');
	}
	
	//處理退款
	public function Refund(Request $request)
	{
		if(Gate::denies('can-refund'))
		{
			return '退款太多次！退款功能被限制！如需退款請聯繫客服！';
		}
		
		$order_id = $request->input('order_id');
		$billing_id = $request->input('billing_id');
		
		$Billing = Billing::findOrFail($billing_id);
		$Billing->status = '請求退款中';
		$Billing->note .= '  於'.date("Y-m-d G:i:s").'請求退款。  訂購編號:'.$order_id.'  帳單編號:'.$billing_id.'<br/>';
		$Billing->save();
		
		return redirect('/member/order/'.$order_id);
	}
	
	//登入
	public function login()
	{
		return view('member.login');
	}
	
	public function loginc(Request $request)
	{
		$name = $request->input('name');
		$pw = $request->input('pw');
		$remember = $request->input('remember');
		
		$User = User::where('name', $name)->first();
		
			if(Auth::attempt(['name' => $name, 'password' => $pw],$remember))
			{
				if($User->userinfo->state == 'open')
				{
					return redirect('/member');
				}
				else
				{
					Auth::logout();
					return '<h1>您的帳號已經被停權囉！<br/>
					已經沒有任何方法可以拯救它！<br/>
					您的帳號違反的我們的服務條款！<br/>
					我們是被「您」逼得要把您帳號給停掉！<br/>
					如果您對於此處分有意見的話(最好是沒有！)請寄到admin@changken.org<br/>
					並請附上:您註冊的帳號、電郵、錢包地址、錢包餘額等帳號資訊<br/>
					我們會進行審核！</h1>';
				}
			}
			else
			{
				return redirect('/member/login');
			}
	}
	
	//登出
	public function logout()
	{
		Auth::logout();
		return redirect('/member/login');
	}
	
	//註冊
	public function reg()
	{
		return view('member.reg');
	}

	public function regc(Request $request)
	{
		$name = $request->input('name');
		$email = $request->input('email');
		$pw = $request->input('pw');
		$pw2 = $request->input('pw2');
		
		if($pw == $pw2)
		{
			$User = new User;
			$User->name = $name;
			$User->email = $email;
			$User->password = bcrypt($pw);
			$User->save();
			
			$UserInfo = new UserInfo;
			$UserInfo->user_id = $User->id;
			$UserInfo->level = 'user';
			$UserInfo->state = 'open';
			$UserInfo->save();
			
			return redirect('/member/login');
		}
		else
		{
			return redirect('/member/reg');
		}
	}
	
	//代碼產生器
	private function randtext($length) 
	{
		$password_len = $length;	//字串長度
		$password = '';
		$word = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';   //亂數內容
		$len = strlen($word);
		for ($i = 0; $i < $password_len; $i++) {
			$password .= $word[rand() % $len];
		}
		return $password;
	}
}
