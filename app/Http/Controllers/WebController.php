<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class WebController extends Controller
{
	//首頁
    public function index()
	{
		return view('index');
	}
	
	//商店
	public function showStore()
	{
		return view('store',[
			'Products' => Product::paginate(5)
		]);
	}
}
