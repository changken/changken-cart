<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Cart;
use App\CartItem;
use App\Order;
use App\OrderItem;
use App\Billing;

class OrderController extends Controller
{
	//結帳
   	public function checkout(Request $request)
	{
		$Cart = Cart::where('user_id', Auth::id())->first();
		$items = $Cart->cartitem; //利用關聯來取得購物車項目
		$total = 0; //初始化總計(產生訂單用)

		//產生訂單
		$Order = new Order;
		$Order->user_id = Auth::id();
		$Order->save();

		//導入購物車項目
		foreach($items as $item)
		{
			$OrderItem = new OrderItem;
			$OrderItem->order_id = $Order->id;
			$OrderItem->product_id = $item->product_id;
			$OrderItem->amount = $item->amount;
			$OrderItem->save();

			$total += $OrderItem->product->price * $OrderItem->amount;
		}

		//刪除購物車項目
		$CartItemDel = CartItem::where('cart_id', $Cart->id);
		$CartItemDel->delete();

		//產生帳單
		$Billing = new Billing;
		$Billing->user_id = Auth::id();
		$Billing->order_id = $Order->id;
		$Billing->price = $total;
		$Billing->status = '未支付';
		$Billing->payway = '';
		$Billing->note = '';
		$Billing->pay_at = null;
		$Billing->save();

		return view('checkout',[
			'Order' => $Order,
			'items' => $Order->orderitem
		]);
	}

	//訂購列表
	public function showOrder()
	{
		return view('member.orderList',[
			'Orders' => Order::where('user_id', Auth::id())->orderBy('id', 'desc')->paginate(5)
		]);
	}

	//單筆訂購的詳細資料
	public function showOrderDetail($o_id)
	{
		$Order = Order::findOrFail($o_id);
		return view('member.orderDetail',[
			'Order' => $Order,
			'items' => $Order->orderitem()->paginate(5)
		]);
	}

	//支付帳單
	public function payBilling(Request $request)
	{
		$Billing = Billing::findOrFail($request->input('billing_id'));
		$payway = $request->input('payway');//支付方式

		if($payway == "changken_wallet") //使用錢包
		{
			$request->session()->put('billing_id', $Billing->id);
			return redirect('/member/order/payByWallet/'.$Billing->id);
		}
		else if($payway == "face2face") //使用面交
		{
			//更新帳單資料
			$Billing->status = '已訂購';
			$Billing->payway = '面交';
			$Billing->note = '請注意！您採用的是非即時金流方式！所以本帳單僅提供通知！不能作為收據！';
			$Billing->pay_at = null;
			$Billing->save();
			return redirect('/member/order/'.$Billing->order->id);
		}
		else if($payway == "free") //使用面交
		{
			//更新帳單資料
			$Billing->status = '免費產品';
			$Billing->payway = '免費產品';
			$Billing->note = '您好！我們會盡快處理您訂單！此帳單不可退款！';
			$Billing->pay_at = date("Y-m-d G:i:s");
			$Billing->save();
			return redirect('/member/order/'.$Billing->order->id);
		}
		else
		{
			return '必須選擇一個支付方式 或者 您的錢包餘額不足！';
		}
	}

	//使用changken 錢包支付
	public function payBillingByWallet($o_id)
	{
		$billing = Billing::findOrFail($o_id);
		$Date = time();

		//產生api token
		$api_token_arr = [
			'Total' => $billing->price,
			'Api_id' => env('API_ID'),
			'Api_key' => env('API_KEY'),
			'Key_pass' => env('KEY_PASS'),
			'Date' => $Date
		];

		ksort($api_token_arr);

		$api_token = hash('sha256', http_build_query($api_token_arr));

		return view('payByWallet',[
			'billing' => $billing,
			'Return_url' => env('APP_URL').'/member/order/payByWalletc',
			'Api_url' => env('API_URL'),
			'Api_id' => env('API_ID'),
			'Api_token' => $api_token,
			'Date' => $Date
		]);
	}

	//返回資訊
	public function payBillingByWalletc(Request $request)
	{
		$data = json_decode($request->input('Response'));
		$id = $request->session()->pull('billing_id');
		$Billing = Billing::findOrFail($id);

		if($data->code == 103)
		{
			//更新帳單資料
			$Billing->status = '已支付';
			$Billing->payway = 'changken 錢包';
			$Billing->note = '訂單編號:'.$data->data->order_id .'  支付人(您):'.$data->data->pay_from .'  支付給:'.$data->data->pay_to .'<br/>';
			$Billing->pay_at = date("Y-m-d G:i:s");
			$Billing->save();

			return redirect('/member/order/'.$Billing->order->id);
		}
		else
		{
			return '錯誤代碼:'.$data->code ."<br/>錯誤資訊:".$data->message."<br/><a href='".env('APP_URL')."/member/order/".$Billing->order->id ."'>再結帳一次</a>";
		}
	}
}
