<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Billing;

class AdminController extends Controller
{	
	public function admin()
	{
		return view('admin.index');
	}
	
	public function showOrder()
	{
		return view('admin.orderList',[
			'Orders' => Order::paginate(5),
		]);
	}

	public function showOrderDetail($o_id)
	{
		$Order = Order::findOrFail($o_id);
		
		return view('admin.orderDetail',[
			'Order' => $Order,
			'items' => $Order->orderitem()->paginate(5),
		]);
	}
	
	public function Refund(Request $request)
	{
		$order_id = $request->input('order_id');
		$billing_id = $request->input('billing_id');
		$response = $request->input('response');
		
		if($response == "Approval") //同意
		{	
			$Billing = Billing::findOrFail($billing_id);
			
			if($Billing->payway == "changken 錢包") //如果使用changken 錢包
			{
				//更改帳單狀態
				$Billing->status = '待確認';
				$Billing->note .= '  已於'.date("Y-m-d G:i:s").'同意退款。請注意！尚未完成退款！請盡速提出您的繳款證明！<br/>';
				$Billing->save();
			}
			else //其他付款方式
			{
				//
			}
		}
		else //不同意
		{
			$Billing = Billing::findOrFail($billing_id);
			$Billing->status = '已支付(退款被拒)';
			$Billing->note .= '  退款被拒  '.date("Y-m-d G:i:s").'<br/>';
			$Billing->save();	
		}
		
		return redirect('/admin/order/'.$order_id);
	}
}
