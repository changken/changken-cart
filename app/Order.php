<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	public function Billing()
	{
		return $this->hasOne(Billing::class);
	}
	
    public function User()
	{
		return $this->belongsTo(User::class);
	}
	
	public function OrderItem()
	{
		return $this->hasMany(OrderItem::class);
	}
	
	public function OrderInfo()
	{
		return $this->hasOne(OrderInfo::class);
	}
}
